<?php


class importer
{

    public function updater($conn, $csvArr)
    {

        $time_pre = microtime(true);
        $conn->query('ALTER TABLE cataloginventory_stock_item DISABLE KEYS');
        $conn->begin_transaction();
        array_shift($csvArr);
        $i = 0;
        foreach ($csvArr as $info) {

            $quer = "UPDATE cataloginventory_stock_item a  INNER JOIN catalog_product_entity b ON b.entity_id = a.product_id SET qty = '".$info[1]."' WHERE sku = '".$info[0]."'";
            $conn->query($quer);
            $i++;
        }

        $conn->commit();
        $conn->query('ALTER TABLE cataloginventory_stock_item ENABLE KEYS');
        $conn->close();
        $time_post = microtime(true);
        $exec_time = $time_post - $time_pre;

        return "Hi dave, my execution time was : ".$exec_time." seconds on date : ".date("Y/m/d").' i processed '.$i.' rows';
    }
}

?>