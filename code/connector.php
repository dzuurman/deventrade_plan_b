<?php


class connector
{
    /**
     * @param $serverName
     * @param $userName
     * @param $password
     * @param $dbName
     * @return false|mysqli
     */
    private $conn;

    public function __construct()
    {
        $servername = '';
        $username = '';
        $password = '';
        $database = '';

        // the connection is stored inside the private variable
        $this->conn = new mysqli($servername, $username, $password, $database);

        if ($this->conn === false) {
            die("ERROR: Could not connect. ".mysqli_connect_error());
        } else {
            return true;
        }
    }

    public function query($sql)
    {
        $result = mysqli_query($this->conn, $sql);
        if ($result) {
            return true;
        } else {
            echo "error encountered with statement : ".$sql;
            die;
        }
    }

    public function begin_transaction()
    {
        $this->conn->begin_transaction();
    }

    public function commit()
    {
        $this->conn->commit();
    }

    public function close()
    {
        $this->conn->close();
    }
}