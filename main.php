<?php


function __autoload($class_name)
{
    require_once "code/" . $class_name.".php";
}

$connection = new connector();
$retriever = new csvRetriever();
$importer = new importer();

$data = $retriever->getCsv('test/elektramat_sku_url_export.csv');
$res = $importer->updater($connection, $data);

echo $res;

?>